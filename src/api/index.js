import request from '@/utils/request'



export const getBannerAPI = () => {
  return request({
    method: "GET",
    url: "/banner?type=2",
  })
}
export const recommendMusicAPI = params => {
  return request({
    url: '/personalized',
    params
  })
}

export const newMusicAPI = params => {
  return request({
    url: '/personalized/newsong',
    params
  })
}

export const hotSerachAPI = () => {
  return request({
    url: '/search/hot',
  })
}
export const searchResultAPI = params => {
  return request({
    url: '/cloudsearch',
    params
  })
}

// 播放页 - 获取歌曲详情
export const getSongByIdAPI = (id) => request({
  url: `/song/detail?ids=${id}`,
  method: "GET"
})

// 播放页 - 获取歌词
export const getLyricByIdAPI = (id) => request({
  url: `/lyric?id=${id}`,
  method: "GET"
})