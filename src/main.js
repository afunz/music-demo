import Vue from 'vue'
import App from './App.vue'
import "@/mobile/flexible"
import "@/styles/reset.css"
import router from "@/router"
import { NavBar, Tabbar, TabbarItem, Col, Row, Image as VanImage, Cell, CellGroup, Icon, Search, Swipe, SwipeItem } from 'vant';




Vue.use(Swipe);
Vue.use(SwipeItem);

Vue.use(Search);
Vue.use(Icon);

Vue.use(Cell);
Vue.use(CellGroup);

Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(NavBar);
Vue.use(Col);
Vue.use(Row);
Vue.use(VanImage);
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
